import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CourseView() {
	const {user} = useContext(UserContext);
	const {courseId} = useParams();
	const navigate = useNavigate();

	const[name, setName] = useState("");
	const[description, setDescription] = useState("");
	const[price, setPrice] = useState(0);

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: "Enroll Success",
					icon: "success",
					text: "You have successfully enrolled for this course!"
				})

				// Redirects back to /courses
				navigate("/courses");
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
			console.log(data);
		})
	};

	useEffect(() => {
		console.log(courseId);
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [courseId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card className="mb-3">
					  <Card.Body className="text-center">
					    <Card.Title>{name}</Card.Title>
					    
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>{description}</Card.Text> 

					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>PHP {price}</Card.Text> 

					    <Card.Subtitle>Class Schedule:</Card.Subtitle>
					    <Card.Text>8 AM to 5 PM</Card.Text>

				      	{
				      		(user.id !== null) ? 
				      			<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
				      		:
				      			<Link className="btn btn-danger" to="/login">Login to Enroll</Link>
				      	}
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
	)
}