import {useState, useEffect, useContext} from 'react';
import {Card} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import ResetPassword from '../components/ResetPassword.js';
import UpdateProfile from '../components/UpdateProfile.js';
import UserContext from '../UserContext';


export default function Profile() {

	// Allows to consume the User context object and its properties to use for use validation
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [password, setPassword] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	
	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		    method: 'GET',
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }})
		.then(response => response.json())
		.then(data => {
		    console.log(data);
		    if(typeof data._id !== "undefined"){
		    	setPassword(data.password);
		        setFirstName(data.firstName);
		        setLastName(data.lastName);
		        setMobileNo(data.mobileNo);
		        setEmail(data.email);
		    }
		    else {
		    	navigate("/");
		    }
		})
	}

	useEffect(() => {
		fetchData();
	}, [])

	return(
		(user.id === null) ?
			<Navigate to="/" />
		:
		<>
			<Card className="mb-3 bg-primary text-white">
			  <Card.Body>
			    <Card.Title className="p-3"><h1>Profile</h1></Card.Title>
			    <Card.Title className="border-bottom p-3"><h1>{firstName} {lastName}</h1></Card.Title>
			    
			    <Card.Subtitle className="pt-3 px-3"><h4>Contacts:</h4></Card.Subtitle>
			    	<ul className="px-4 mx-3">
			    		<li>Email: {email}</li>
			    		<li>Mobile No: {mobileNo}</li>
			    	</ul>
			  </Card.Body>
			</Card>	

			<Card className="mb-3 bg-primary text-white">
			  <Card.Body>
			    <ResetPassword />
			  </Card.Body>
			</Card>	

			<Card className="mb-3 bg-primary text-white">
			  <Card.Body>
			    <UpdateProfile fetchData={fetchData}/>
			  </Card.Body>
			</Card>	
		</>
	)
}