import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

	// Allows to consume the User context object and its properties to use for use validation
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(true);
	const navigate = useNavigate();
	const authenticate = (event) => {
		//prevents page reloading
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response => response.json()).then(result => {
			console.log(result.access);
			if(result.access) {
				localStorage.setItem('token', result.access);
				retrieveUserDetails(result.access);
				// setUser({
				// 	access: localStorage.getItem('token')
				// })
				Swal.fire({
					title: "Login successful",
					icon: "success",
					text: "Welcome to Zuitt Coding Bootcamp!"
				})
			}
			else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})
		setEmail("");
		setPassword("");
	};

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: 'GET',
			headers: {
				'Authorization': `Bearer ${token}`
			}})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	};


	useEffect(() => {
		if((email !== "" && password !== "")) setIsActive(false);
		else setIsActive(true);
	}, [email, password]);

	return(
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
			<Form onSubmit = {event => authenticate(event)}>
	        	<h1 className="my-5 text-center">Login</h1>     
	            <Form.Group className = "mb-3">
	                <Form.Label>Email:</Form.Label>
	                <Form.Control 
		                type="email" 
		                placeholder="Enter Email" 
		                value = {email}
		                onChange = {event => {setEmail(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Password:</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Enter Password" 
		                value = {password}
		                onChange = {event => {setPassword(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Button variant="primary" type="submit" disabled={isActive === true}>Submit</Button>  
	        </Form>
	)
}