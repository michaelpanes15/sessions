import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register() {

	// Allows to consume the User context object and its properties to use for use validation
	const {user} = useContext(UserContext);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	const registerUser = (event) => {
		//prevents page reloading
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then(response => response.json()).then(result => {
			console.log(result);

			if(result) {
				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setConfirmPassword("");
				alert("User Successfully Registered!");
			}
			else alert("Error Occured, Please try Again")
			
		})
	}

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") 
			&& (confirmPassword === password) && (mobileNo.length === 11) && (email.includes("@"))) setIsActive(true);
		else setIsActive(false);
	}, [firstName, lastName, email, mobileNo, password, confirmPassword]);

	return(
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
			<Form onSubmit = {event => registerUser(event)}>
	        	<h1 className="my-5 text-center">Register</h1>
	            <Form.Group className = "mb-3">
	                <Form.Label>First Name:</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter First Name" 
		                value = {firstName}
		                onChange = {event => {setFirstName(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Last Name:</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter Last Name" 
		                value = {lastName}
		                onChange = {event => {setLastName(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Email:</Form.Label>
	                <Form.Control 
		                type="email" 
		                placeholder="Enter Email" 
		                value = {email}
		                onChange = {event => {setEmail(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Mobile No:</Form.Label>
	                <Form.Control 
		                type="number" 
		                placeholder="Enter 11 Digit No." 
		                value = {mobileNo}
		                onChange = {event => {setMobileNo(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Password:</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Enter Password" 
		                value = {password}
		                onChange = {event => {setPassword(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Confirm Password:</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Confirm Password" 
		                value = {confirmPassword}
		                onChange = {event => {setConfirmPassword(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Button variant="primary" type="submit" disabled={isActive === false}>Submit</Button>  
	        </Form>
	)
}