import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	// Allows to consume the User context object and its properties to use for use validation
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState(false);

	const registerCourse = (event) => {
		//prevents page reloading
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);

			if(result) {
				Swal.fire({
					title: "Course Added",
					icon: "success",
					text: "Course has been successfully been created!"
				})
				navigate("/courses");

			}
			else {
				Swal.fire({
					title: "Unssuccessful Course Creation",
					icon: "error",
					text: "Please try again"
				})
			}
			
			setName("");
			setDescription("");
			setPrice("");
		})
	}

	useEffect(() => {
		if(name !== "" && description !== "" && price !== "") setIsActive(false);
		else setIsActive(true);
	}, [name, description, price]);

	return(
		(user.id === null || user.isAdmin === false) ?
			<Navigate to="/courses" />
		:
			<Form onSubmit = {event => registerCourse(event)}>
	        	<h1 className="my-5 text-center">Add Course</h1>
	            <Form.Group className = "mb-3">
	                <Form.Label>Name:</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter Course Name" 
		                value = {name}
		                onChange = {event => {setName(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Description:</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter Description" 
		                value = {description}
		                onChange = {event => {setDescription(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            <Form.Group className = "mb-3">
	                <Form.Label>Price:</Form.Label>
	                <Form.Control 
		                type="number" 
		                placeholder="Enter Price" 
		                value = {price}
		                onChange = {event => {setPrice(event.target.value)}}
		                required
	                />
	            </Form.Group>
	            
	            <Button variant="primary" type="submit" disabled={isActive === true}>Submit</Button>  
	        </Form>
	)
}