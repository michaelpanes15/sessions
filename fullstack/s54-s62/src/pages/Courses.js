import {useEffect, useState, useContext} from 'react';
import UserView from '../components/UserView.js'
import AdminView from '../components/AdminView.js';
import UserContext from '../UserContext';

export default function Courses() {

	const [courses, setCourses] = useState([]);

	const {user} = useContext(UserContext)

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data);
		})
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		(user.isAdmin) ?
			<AdminView coursesData={courses} fetchData={fetchData}/>
		:
			<UserView coursesData={courses}/>
		
	)
}