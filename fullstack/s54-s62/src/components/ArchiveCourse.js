import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function ArchiveCourse({course, fetchData, isActive}) {

	const archiveToggle = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Course successfully archived"
				})
				fetchData();
			}
			else {
				Swal.fire({
					title: "Failed!",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	const activateToggle = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Course successfully activated"
				})
				fetchData();
			}
			else {
				Swal.fire({
					title: "Failed!",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	return(
		(isActive) ?
			<Button variant="danger" size="sm" onClick={() => archiveToggle(course)}>Archive</Button>
		:
			<Button variant="success" size="sm" onClick={() => activateToggle(course)}>Activate</Button>
	)
}