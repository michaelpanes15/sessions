import React, { useState } from 'react';
import CourseCard from './CourseCard.js';

const CourseSearch = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [courses, setCourses] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          minPrice: parseFloat(minPrice),
          maxPrice: parseFloat(maxPrice),
        }),
      });

      if (response.ok) {
        const data = await response.json();
        setCourses(data.courses);
      } else {
        console.error('Error fetching courses');
      }
    } catch (error) {
      console.error('Error fetching courses', error);
    }
  };

  return (
    <div className="container px-0">
      <h2>Search Courses by Price</h2>
      <div className="mb-3">
        <label htmlFor="minPrice" className="form-label">
          Minimum Price
        </label>
        <input
          type="number"
          className="form-control"
          id="minPrice"
          value={minPrice}
          onChange={(e) => setMinPrice(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="maxPrice" className="form-label">
          Maximum Price
        </label>
        <input
          type="number"
          className="form-control"
          id="maxPrice"
          value={maxPrice}
          onChange={(e) => setMaxPrice(e.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleSearch}>
        Search
      </button>

      <h3>Search Results:</h3>
      <ul>
        {courses.map((course) => (
          <li key={course.id}>{course.name} - {course.price}</li>
        ))}
      </ul>
    </div>
  );
};

export default CourseSearch;
