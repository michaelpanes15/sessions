import { useState, useEffect} from 'react';
import {CardGroup} from 'react-bootstrap';
import PreviewCourses from './PreviewCourses';



export default function FeaturedCourses(){
	const [previews, setPreviews] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			const numbers = [];
			const featured = [];

			const generateRandomNums = () => {
				let randomNum = Math.floor(Math.random() * data.length);
				if(numbers.indexOf(randomNum) === -1) {
					numbers.push(randomNum)
				}
				else {
					generateRandomNums();
				}
			}
			console.log(data);
			for(let i=0; i < ((data.length < 5) ? data.length: 5); i++){
				generateRandomNums();
				featured.push(
					<PreviewCourses data={data[numbers[i]]} key={i} breakPoint={2}/>
				)
			};

			setPreviews(featured);
		})
	}, [])

	return (
		<>
			<h2 className="text-center">Featured Courses</h2>
			<CardGroup className="justify-content-center">
				{previews}
			</CardGroup>
		</>
	)
}