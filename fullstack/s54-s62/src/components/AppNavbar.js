import {useContext} from 'react';
import {Navbar, Container, Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar(){
	// Allows to consume the User context object and its properties to use for use validation
	const {user} = useContext(UserContext);

	const renderLinks = () => {
        if (user.id !== null) {
            return user.isAdmin ? (
                <>
                    <Nav.Link as={NavLink} to="/courses">Admin Dashboard</Nav.Link>
                    <Nav.Link as={NavLink} to="/addCourse">Add Course</Nav.Link>
                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                </>
            ) : (
                <>
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                    <Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                </>
            );
        } else {
            return (
                <>  
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                </>
            );
        }
    };

    return (
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand as={Link} to="/" expand="xs">Zuitt Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        {renderLinks()}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

