import { useState, useEffect } from 'react';
import CourseCard from '../components/CourseCard.js';
import CourseSearch from '../components/CourseSearch.js';
import CourseSearchByPrice from '../components/CourseSearchByPrice.js';

export default function UserView(props){
  const userCourses = props.coursesData;
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    setCourses(userCourses.filter(course => course.isActive === true).map((course_item) => {
      return(
        <CourseCard key = {course_item._id} course = {course_item}/>
      )
    }));
  }, [userCourses])

  return (
    <>
      <h1 className="text-center">Courses</h1>
      <CourseSearch />
      <CourseSearchByPrice />
      <h2 className="">Course List</h2>
      {courses}
    </>
  )
}