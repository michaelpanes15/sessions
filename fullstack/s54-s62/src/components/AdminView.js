import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import EditCourse from './EditCourse.js';
import ArchiveCourse from './ArchiveCourse.js';

export default function AdminView({coursesData, fetchData}) {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    const mappedCourses = coursesData.map(course => (
      <tr key={course._id}>
        <td>{course._id}</td>
        <td>{course.name}</td>
        <td>{course.description}</td>
        <td>{course.price}</td>
        <td className={(course.isActive) ? "text-success" : "text-danger"}>
         {(course.isActive) ? "Available" : "Unavailable"}
        </td>
        <td><EditCourse course={course._id} fetchData={fetchData}/></td>
        <td><ArchiveCourse course={course._id} fetchData={fetchData} isActive={(course.isActive)}/></td>
      </tr>
    ));

    setCourses(mappedCourses);
  }, [coursesData, fetchData]);

  return (
    <>
      <h1 className="text-center">Admin Dashboard</h1>
      <Table className="table table-striped table-bordered table-hover table-responsive">
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan="3">Actions</th>
          </tr>
        </thead>
        <tbody>
          {courses}
        </tbody>
      </Table>
    </>
  );
}
