import {Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function CourseCard({course}){
  const { name, description, price, _id } = course;

  return(
    <Card className="mb-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text> 

        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PHP {price}</Card.Text>

          <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
  )
}

// PropTypes for data validation
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  }).isRequired 
}