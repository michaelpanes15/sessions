import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar.js';
import AddCourse from './pages/AddCourse.js';
import Courses from './pages/Courses.js';
import CourseView from './pages/CourseView.js';
import Error from './pages/Error.js'
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import Profile from './pages/Profile.js';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    })

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        console.log(user);
        console.log(localStorage);
    }, [user])

    // Efficient hot reload
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }})
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(typeof data._id !== "undefined"){
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            }
            else {
                setUser({
                    id: null,
                    isAdmin: null
                });
            }
        })
    }, [])

    return (
        <UserProvider value={{user, setUser, unsetUser }}>
            <Router>
                <Container>
                    <AppNavbar/>
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/courses" element={<Courses />} />
                        <Route path="/courses/:courseId" element={<CourseView />} />
                        <Route path="/addCourse" element={<AddCourse />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="/login" element={<Login />} /> 
                        <Route path="/logout" element={<Logout />} />
                        <Route path="/profile" element={<Profile />} />
                        <Route path="*" element={<Error />} />
                    </Routes>
                </Container>
            </Router>
        </UserProvider>
        
  );
}

export default App;
