const firstNum = 8 ** 2;
console.log(firstNum);
console.log(firstNum);
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

let name = "Ken";

let message = "Hello " + name + "Welcome to Programming!";
console.log("Message without template literals: " + message);

let message2 = `Hello ${name} Welcome to Programming!`;
console.log(`Message with template literals: ${message}`);

const anotherMessage = `
${name} attended a Math Competition. 
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest onnyou savings amount is: ${principal * interestRate}`);

const fullName = ["Juan", "Dela", "Cruz"];
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

const  [firstName, middleName, lastName] = fullName;
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

const {givenName, familyName, maidenName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}! Its good to see you.`);

function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

const hello = () => {
	console.log("Hello World");
}

hello();

// function printFullName(fName, mName, lName) {
// 	console.log(fname + " " + mName + " " + lName);
// }

// printFullName("john", "d", "sith");

const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`);
} 

printFullName("john", "d", "sith");

const students = ['john','d', 'smith'];

students.forEach(function(student){
	console.log(`${student} is a student`);
})

students.forEach((student) => {
	console.log(`${student} is a student`);
})

function add(x, y) {
	return x + y;
}

let total = add(2,4);
console.log(total);

let difference = (x, y) => x+y;
console.log(difference(2,4));

const greet = (name='User') => {
	return `Good Afternoon ${name}`;
}

console.log(greet());
console.log(greet("Judy"));


class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const fordExplorer = new Car();
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

const toyotaVios = new Car("Toyota", "Vios", 2018);
console.log(toyotaVios);