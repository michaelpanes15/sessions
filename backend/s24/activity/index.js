let value = prompt("Provide a number");
console.log("The number you provided is " + value + ".");
for(let count = value; count >= 0; count--){
	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop.");
		break;
	} 
	if(count % 10 == 0) console.log("The number is divisible by 10. Skipping the number.");
	if(count % 5 == 0 && count % 10 != 0) console.log(count);
}


let string = "supercalifragilisticexpialidocious";
let filteredString = "";
console.log(string);
for(let index = 0; index < string.length; index++) {
	if(string[index].toLowerCase() == "a" ||
	   string[index].toLowerCase() == "e" ||
	   string[index].toLowerCase() == "i" ||
	   string[index].toLowerCase() == "o" ||
	   string[index].toLowerCase() == "u") continue;
	else  filteredString += string[index]; 
}
console.log(filteredString);