const Product = require('../models/Product.js');

module.exports.registerProduct = (request, response) => {
	return Product.find({name: request.body.name}).then((result, error) => {
		if(error) return response.status(500).send({message: error.message})
		if(result.length > 0) return response.status(400).send({message:"Product Exists"});
		let newProduct = new Product({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		})
		return newProduct.save().then((registered_product, error) => {
			if(error) return response.status(500).send({message: error.message})
			return response.status(201).send({message: "Successfully registered a product!", data: registered_product});
		})
	})
}

module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then((result) => {
		return response.status(200).send(result);
	}).catch(error => response.status(500).send(error));
}

module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		let selectedProduct = result.map(product => {
		return {
			Id: product._id,
			Name: product.name,
			Description: product.description,
			Price: product.price
		}
		});
		return response.status(200).send(selectedProduct);
	}).catch(error => response.status(500).send(error));
}

module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.status(200).send(result);
	}).catch(error => response.status(500).send(error));
}

module.exports.searchProduct = (request, response) => {
	return Product.find({name: { $regex: request.body.name, $options: 'i' }}).then((result, error) => {
		if(error) return response.status(500).send({message: error.message});
		if(result.length <= 0) return response.status(400).send({message: "Search not found"});
		
		let selectedProduct;
		if(request.user.isAdmin) {
			selectedProduct = result.map(product => {
			return {
				Name: product.name,
				Description: product.description,
				Price: product.price
			}})
		}
		else {
			selectedProduct = result.filter(product => {
				return product.isActive;
			}).map(product => {
				return {
				Name: product.name,
				Description: product.description,
				Price: product.price
			}})
		}
		
		return response.status(200).send(selectedProduct);
	})
}

module.exports.updateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, {
		$set: {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		}
	}, {new: true}).then(result => {
		return response.status(200).send({message: "Product has been updated successfully", data: result});
	}).catch(error => {
		return response.status(500).send({message: error.message});
	})
}

module.exports.activateProduct = (request, response) => {
  return Product.findByIdAndUpdate(request.params.id, {
    $set: {
      isActive: true,
    },
  }).then((result, error) => {
    if (error) return response.send({ message: error.message });
    return response.send(true);
  });
};

module.exports.archiveProduct = (request, response) => {
  return Product.findByIdAndUpdate(request.params.id, {
    $set: {
      isActive: false,
    },
  }).then((result, error) => {
    if (error) return response.send({ message: error.message });
    return response.send(true);
  });
};