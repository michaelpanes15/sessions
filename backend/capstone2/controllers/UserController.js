const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.registerUser = (request, response) => {
	return User.find({email: request.body.email}).then((result, error) => {
		if(error) return response.status(500).send({message: error.message});
		if(result.length > 0) return response.status(400).send({message: "Email Exists"}); 
		else {
			let new_user = new User({
				userName: request.body.userName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10)
			})
			return new_user.save().then((registered_user, error) => {
				if(error) return response.status(500).send({message: error.message})
					registered_user.password = "";
				return response.status(201).send({
					message: "Successfully registered a user!", 
					data: registered_user
				})
			}).catch(error => console.log(error));
		}
	})
}

module.exports.updateUser = (request_body) => {
	return User.findOne({email: request_body.email}).then((result, error) => {
		if(error) return {status: 500, message: error.message};
		if(result == null) return {status: 400, message: "No User Found"};
		
		result.userName = request_body.userName || result.username;
		result.email = request_body.email || result.email;
		result.password = bcrypt.hashSync(request_body.password, 10) || result.password; 
		
		return result.save().then(updated_user => {
			updated_user.password = "";
			if(result != null) return {status: 201, message: updated_user};
		}).catch(error => error.message);
		
		
	})
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then((result) => {
		if(result == null) return "User is not Registered";
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
		if(isPasswordCorrect) {
			//return response.send({accessToken: auth.createAccessToken(result)})
			return {accessToken: auth.createAccessToken(result), id: result._id, meesage: true}
		}
		else return {message: "Incorrect Password"}
	}).catch(error => {message: error});
}

module.exports.getProfile = (request_body) => {
	return User.findById(request_body.id).then((result, error) => {
		if(error) return {message: error.message}
		result.password = "";
		return result;
	})
}

module.exports.setAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.body.id, {isAdmin: true}).then((error, result) => {
		if(error) return response.status(500).send({message: error.message});
		if(!result) return response.status(400).send({message: "User does not exist"});
	})
}

module.exports.addToCart = async (request, response) => {
	if(request.user.isAdmin) return response.send('Action Forbidden');

	let productPrice = await Product.findById(request.body.productId).then(product => {
		if(product == null || !product.isActive) return {status: false, message: "Product does not exist"};
		return product.price;
	}).catch(error => error.message);

	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let isProductAlreadyInCart = user.orderCarts.find(order => order.productId === request.body.productId);
		if(isProductAlreadyInCart) return {status: false, message: "Product is already in cart."}

		let newProductOrder = {
			productId: request.body.productId,
			quantity: request.body.quantity,
			price: productPrice,
			subTotal: productPrice * request.body.quantity
		}
		user.orderCarts.push(newProductOrder);
		return user.save().then(updated_cart => true).catch(error => {return {message: error.message}});
	})

	if(isUserUpdated !== true) return response.status(400).send(isUserUpdated);

	if(isUserUpdated) return response.status(201).send({message: "Product Successfully added in cart"});
}

module.exports.createOrder = async (request, response) => {
	if(request.user.isAdmin) return response.send('Action Forbidden');
	let isUserHavePendingOrder = await Order.findOne({userId: request.user.id, orderStatus: "Pending"}).then(result => {
		if(result) return {status: true, message: "User already have pending order!"};
		else return {status: false}
	})
	if(isUserHavePendingOrder.status) return response.status(400).send(isUserHavePendingOrder.message);

	let newOrderList = [];
	let isUserUpdated = await User.findById(request.user.id).then(user => {
		request.body.orders.forEach(orderId => {
			let cartIndex = user.orderCarts.findIndex(product => product.productId == orderId);
			if(cartIndex == -1) return response.status(400).send({status: false, message: "Product not in cart"});
			
			newOrderList.push(user.orderCarts[cartIndex]);
			user.orderCarts.splice(cartIndex, 1);
		});
		return user.save().then(updated_cart =>{
			return {status: true}
		}).catch(error => { return {status: false, message: error.message}});
	});
	if(isUserUpdated.status !== true) return response.status(500).send(isUserUpdated.message);
	let newOrdersListSubtotal = await newOrderList.map(order => order.subTotal).reduce((x, y) => x+y);
	let new_order = new Order({
		userId: request.user.id,
		products: newOrderList,
		totalAmount: newOrdersListSubtotal
	});
	return new_order.save().then(registered_order => {
		return response.status(201).send({message: "Successfully created order!", data: registered_order})
	}).catch(error => error.message);
}

module.exports.getCart = (request, response) => {
	return User.findById(request.user.id).then((user, error) => {
		if(error) return response.send({message: error.message});
		if(user.orderCarts.length == 0) return response.status(400).send({message: "User has no products in cart"});
		return response.status(200).send({userId: request.user.id, cart: user.orderCarts});
	})
}

module.exports.editCart = (request, response) => {
	return User.findById(request.user.id).then(user => {
		if(user.orderCarts.length <= 0) return response.status(400).send({message: "User has no item in cart"})
		let productIndex = user.orderCarts.findIndex(product => product.productId === request.body.id);
		user.orderCarts[productIndex] = {
			productId: user.orderCarts[productIndex].productId,
			subTotal: (user.orderCarts[productIndex].subTotal/user.orderCarts[productIndex].quantity) * request.body.quantity,
			quantity: request.body.quantity
		}
		return user.save().then(updated_cart => {
			return response.status(200).send({message: "Cart has successfully been updated", data: user.orderCarts[productIndex]});
		}).catch(error => response.status(500).send({message: error.message}));
	})
}

module.exports.removeCart = (request, response) => {
	return User.findById(request.user.id).then(user => {
		if(user.orderCarts.length <= 0) return response.status(400).send({message: "User has no item in cart"})
		let productIndex = user.orderCarts.findIndex(product => product.productId == request.params.id);
		user.orderCarts.splice(productIndex, 1);
		return user.save().then(updated_cart => {
			return response.status(200).send({message: "Item has successfully been removed from cart", data: updated_cart.orderCarts});
		}).catch(error => response.status(500).send({message: error.message}));
	})
}

module.exports.clearCart = (request, response) => {
	return User.findById(request.user.id).then(user => {
		if(user.orderCarts.length <= 0) return response.status(400).send({message: "User has no item in cart"})
		user.orderCarts = [];
		return user.save().then(updated_cart => {
			return response.status(200).send({message: "Cart has successfully been cleared", data: updated_cart.orderCarts});
		}).catch(error => response.status(500).send({message: error.message}));
	})
}

