let Order = require('../models/Order.js');

module.exports.getAllOrders = (request, response) => {
	return Order.find({}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.getAllActiveOrders = (request, response) => {
	return Order.find({orderStatus: "Active"}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Active Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.getAllPendingOrders = (request, response) => {
	return Order.find({orderStatus: "Pending"}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Pending Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.getAllClosedOrders = (request, response) => {
	return Order.find({orderStatus: "Closed"}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Closed Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.getAllUserOrders = (request, response) => {
	return Order.find({userId: request.user.id}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.getAllUserActiveOrders = (request, response) => {
	return Order.find({userId: request.user.id, orderStatus: "Active"}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Active Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.getAllUserPendingOrders = (request, response) => {
	return Order.find({userId: request.user.id, orderStatus: "Pending"}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Pending Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.getAllUserClosedOrders = (request, response) => {
	return Order.find({userId: request.user.id, orderStatus: "Closed"}).then(orders => {
		if(orders.length <= 0) return response.status(200).send({message: "No Closed Orders Found"});
		return response.status(200).send(orders);
	}).catch(error => error.message);
}

module.exports.activateOrder = (request, response) => {
	return Order.findOne({userId: request.user.id}).then(order => {
		if(order == null) return response.status(400).send({message: "User does not have a pending order"});
		if((request.body.paymentDetails === "" || request.body.paymentDetails === null || request.body.paymentDetails === undefined) || 
			(request.body.deliveryAddress === "" || request.body.deliveryAddress === null || request.body.deliveryAddress === undefined)) {
			return response.status(400).send({message: "Please fill up Payment Details and Delivery Address"});
		}
		order.paymentDetails = request.body.paymentDetails;
		order.deliveryAddress = request.body.deliveryAddress;
		order.orderStatus = "Active";
		order.purchasedOn = new Date();
		return order.save().then(result => {
			response.status(201).send({message: "Payment Successfull", data: result})
		})
	})
}

module.exports.closeOrder = (request, response) => {
	return Order.findOne({userId: request.user.id}).then(order => {
		if(order == null) return response.status(400).send({message: "User does not have a pending order"});
		if(order.orderStatus != "Active") return response.status(400).send({message: "Order is not active"});
		order.orderStatus = "Closed";
		return order.save().then(result => {
			response.status(201).send({message: "Order has successfully been closed", data: result})
		})
	})
}