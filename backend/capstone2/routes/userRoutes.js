const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController.js');
const auth = require('../auth.js');

router.post('/register', (request, response) => {
	UserController.registerUser(request, response)
})

router.put('/update', (request, response) => {
	UserController.updateUser(request.body).then(result => {
		response.status(result.status).send(result.message);
	})
})

router.post('/login', (request, response) => {
	UserController.loginUser(request, response).then(result => {
		response.send(result);
	});
})

router.post("/details", auth.verify, (req, res) => {
  UserController.getProfile(req.body).then(result => {
    res.send(result);
  })
})

router.get('/logout', auth.verify, (request, response) => {
	response.send("Successfully logout!");
})

router.put('/set-admin', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.setAdmin(request, response);
})

router.get('/get-cart', auth.verify, (request, response) => {
	UserController.getCart(request, response);
})

router.post('/edit-cart', auth.verify, (request, response) => {
	UserController.editCart(request, response);
})

router.get('/remove-cart/:id', auth.verify, (request, response) => {
	UserController.removeCart(request, response);
})

router.get('/clear-cart', auth.verify, (request, response) => {
	UserController.clearCart(request, response);
})

module.exports = router;