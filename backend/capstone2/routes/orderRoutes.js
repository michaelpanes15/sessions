const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js');
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

router.get('/admin/all', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getAllOrders(request, response);
})

router.get('/admin/pending', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getAllPendingOrders(request, response);
})

router.get('/admin/active', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getAllActiveOrders(request, response);
})

router.get('/admin/closed', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getAllClosedOrders(request, response);
})

router.get('/user/all', auth.verify, (request, response) => {
	OrderController.getAllUserOrders(request, response);
})

router.get('/user/pending', auth.verify, (request, response) => {
	OrderController.getAllUserPendingOrders(request, response);
})

router.get('/user/active', auth.verify, (request, response) => {
	OrderController.getAllUserActiveOrders(request, response);
})

router.get('/user/closed', auth.verify, (request, response) => {
	OrderController.getAllUserClosedOrders(request, response);
})

router.post('/create', auth.verify, (request, response) => {
	UserController.createOrder(request, response);
})

router.put('/activate', auth.verify, (request, response) => {
	OrderController.activateOrder(request, response);
})

router.put('/close', auth.verify, (request, response) => {
	OrderController.closeOrder(request, response);
})

module.exports = router