//Server Variables
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const app = express();
const cors = require('cors');
const port = process.env.PORT || 4000;
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');

//Database Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-panes.zwprpr3.mongodb.net/b303-ecommerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let database = mongoose.connection;
database.on("error", () => console.log('Connection error:('));
database.once('open', () => console.log('Connected to MongoDB!'));

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);

app.listen(port, () => {
	console.log(`E-Commerce API is now running at localhost:${port}`);
})

module.exports = app;


