//Server Variables
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const app = express();
const port = 4000;

//mongoDb
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-panes.zwprpr3.mongodb.net/b303-todo-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let database = mongoose.connection;

database.on("error", () => console.log('Connection error:('));
database.once('open', () => console.log('Connected to MongoDB!'));


//Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Mongoose Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})



//Mongoose Models
const Task = mongoose.model("Task", taskSchema);

//Mongoose Routes
app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}).then((result) => {
		if(result != null && result.name == request.body.name) {
			return response.send("Duplicate task found");
		} 
		else {
			//Creates new instance
			let newTask = new Task({
				name: request.body.name
			});

			//Save new instance to database
			newTask.save().then((savedTask, error) => {
				if(error) return response.send({message: error.message})
				return response.send(201, 'New Task Created');
			})
		}}
	)
})

app.get('/tasks', (request, response) => {
	Task.find({}).then((result, error) => {
		if(error) return response.send({message: error.message});
		return response.status(200).json({tasks: result})
	})
})



const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

const User = mongoose.model("User", userSchema);

app.post('/users', (request, response) => {
	response.send(request.body);
	
})

//Server Listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`));


module.exports = app;


