function login(username, password, role) {
	if(!username || !password || !role) {
		return "Inputs must not be empty";
	}
	else if(role == "teacher"){
		return "Thank you for logging in, teacher";
	}
	else if(role == "rookie"){
		return "Welcome to the class portal, student!";
	}
	else if(role == "admin"){
		return "Welcome back to the class portal, admin";
	}
	else {
		return "Role out of range";
	}
}

function checkAverage(firstNumber, secondNumber, thirdNumber, fourthNumber){
	let averageScore = (firstNumber + secondNumber + thirdNumber + fourthNumber) / 4;

	if(averageScore <= 74) {
		return "Hello, student, you average is: " + averageScore + ". The Letter equivalent is F"
	}
	else if(averageScore >= 75 && averageScore <= 79) {
		return "Hello, student, you average is: " + averageScore + ". The Letter equivalent is D"
	}
	else if(averageScore >= 80 && averageScore <= 84) {
		return "Hello, student, you average is: " + averageScore + ". The Letter equivalent is C"
	}
	else if(averageScore >= 85 && averageScore <= 89) {
		return "Hello, student, you average is: " + averageScore + ". The Letter equivalent is B"
	}
	else if(averageScore >= 90 && averageScore <= 95) {
		return "Hello, student, you average is: " + averageScore + ". The Letter equivalent is A"
	}
	else if(averageScore >= 96) {
		return "Hello, student, you average is: " + averageScore + ". The Letter equivalent is A+"
	}
}

function login(username, password, role) {
	if(!username || !password || !role) {
		return "Inputs must not be empty";
	}
}
