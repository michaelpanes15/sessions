db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: '$supplier_id', avgStocks:{$avg: '$stock'}}},
  {$project: {_id:0}} 
]);

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:'$supplier_id', maxPrice:{$max:'$price'}}},
  {$sort: {maxPrice:-1}}
])

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:'$supplier_id', maxPrice:{$max:'$price'}}},
  {$sort: {maxPrice:1}}
])

db.fruits.aggregate([
  {$unwind: '$origin'}
])

db.fruits.aggregate([
  {$unwind: '$origin'},
  {$group: {_id: '$origin', kinds: {$sum:1}}}
])