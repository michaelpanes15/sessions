//Server Variables
const express = require("express");
const app = express();
const port = 4000;

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));


//Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`));

app.get('/', (request, response) => {
	response.send("Hello World");
})

app.post('/greeting', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`)
})

let users = [];
app.post('/register', (request, response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} has successfully been registered!`);
	}
	else {
		response.send(`Please input both username and password`);
	}
})

app.get('/users', (request, response) => {
	response.send(users);
})

app.delete('/delete-user', (request, response) => {
	let index = users.findIndex((user) => user.username === request.body.username);
	console.log(index);
	if(users.length == 0) response.send(`No Users Found`);
	else if(index > -1){
		users.splice(index, 1);
		response.send(`User ${request.body.username} has been deleted.`);
	}
	else response.send(`User ${request.body.username} does not exist.`);
})

app.delete('/delete-users', (request, response) => {
	let message;
	if(users.length > 0){
		for(let index=0; index<users.length; index++){
			if(request.body.username == users[index].username){
				users.splice(index, 1);
				message = `User ${request.body.username} has been deleted.`;
				break;
			}
		}
		if(typeof message === 'undefined') message = `User does not exist`;
	}  
	else message = "No Users Found";
	response.send(message);
})

module.exports = app;