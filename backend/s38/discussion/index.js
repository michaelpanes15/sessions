let http = require('http');

const port = 4000;

let users = [
	{
		"name": "Paolo",
		"email": "paolo@gmail.com"	
	},
	{
		"name": "Shinji",
		"email": "shinji@gmail.com"	
	}
];

const app = http.createServer((req, res) => {
	if(req.url == '/items' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Data retrieved");
	}
	else if(req.url == '/items' && req.method == 'POST'){
		res.writeHead(201, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(users));
		res.end("Data Sent");
	}
	else if(req.url == '/users' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(users));
	}
	else if(req.url == '/users' && req.method == 'POST'){
		let request_body = ``;
		req.on('data', (data) => {
			request_body += data;
		})
		res.writeHead(201, {'Content-Type': 'application/json'});
		req.on('end', () => {
			console.log(typeof request_body);
			request_body = JSON.parse(request_body);
			let new_user = {
				"name": request_body.name,
				"email": request_body.email,
			};
			users.push(new_user);
			res.end(JSON.stringify(new_user));
		})
	}
	else if(req.url == '/archivecourses' && req.method == 'DELETE'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Archive courses to our resources");
	}
})

app.listen(port, () => console.log(`Server is running at localhost:${port}`));
