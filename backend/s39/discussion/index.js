//get
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => {
	console.log(data);
});

// async function fetchData() {
// 	let result =  await fetch('https://jsonplaceholder.typicode.com/posts');
// 	let jsonResult = await result.json();
// 	console.log(jsonResult);
// }

//add
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json',	
	},
	body: JSON.stringify({
		title: "new POst",
		body: "new Body",
		userId: 2
	})
})
.then((response) => response.json())
.then((data) => console.log(data));

//update
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json',	
	},
	body: JSON.stringify({
		title: "Updated POst",
	})
})
.then((response) => response.json())
.then((data) => console.log(data));

//delete
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data));

//filter
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((data) => console.log(data));

//comments
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((data) => console.log(data));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
         method: 'DELETE'
      })
      .then((response) => response.json())
      .then((data) => console.log(data));

// const port = 4000;
// const app = http.createServer((req, res) => {

// })

// app.listen(port, () => `Connecting to server with port ${port}`);


//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await 
       (fetch('https://jsonplaceholder.typicode.com/todos/1')
              .then((response) => response.json())
              .then((json) => {
                   console.log(json);
                   return json;
              }));

}



// Getting all to do list item
async function getAllToDo(){

   return await 
      (fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      .then((data) => {
         console.log(data.map((entry) => entry.title));
         return data.map((entry) => entry.title);
      }))
      ;

}

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await 

       //Add fetch here.
      (fetch('https://jsonplaceholder.typicode.com/todos/1')
                  .then((response) => response.json())
                  .then((json) => {
                  console.log(json);
                  return json;
                  }))

   ;

}

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await 

       //Add fetch here.
      (fetch('https://jsonplaceholder.typicode.com/todos', {
                     method: 'POST',
                     headers: {
                        'Content-Type': 'application/json'
                     },
                     body: JSON.stringify({
                        title: 'something',
                        userId: 1,
                        completed: false,
                        id: 201
                     })
                  })
                  .then((response) => response.json())
                  .then((json) => {
                  console.log(json);
                  return json;
             }))


   ;

}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await 

       //Add fetch here.
      (fetch('https://jsonplaceholder.typicode.com/todos/1', {
                     method: 'PUT',
                     headers: {
                        'Content-Type': 'application/json'
                     },
                     body: JSON.stringify({
                      title: "Sample title",
                      description: "Sample description for the activity",
                      status: true,
                      dateCompleted: "2023-07-31 16:15:41",
                      userId: 2
                     })
                  })
                  .then((response) => response.json())
                  .then((json) => {
                  console.log(json);
                  return json;
             }))


   ;

}

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await 
      (fetch('https://jsonplaceholder.typicode.com/todos/1', {
                     method: 'DELETE'
                  })
                  .then((response) => response.json())
                  .then((json) => {
                  console.log(json);
                  return json;
             }))

      ;
   

}


//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}