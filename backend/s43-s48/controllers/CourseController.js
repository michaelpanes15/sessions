const Course = require('../models/Course.js');

module.exports.registerCourse = (request_body) => {
	return Course.find({name: request_body.name}).then((result, error) => {
		if(error) return {message: error.message}
		if(result.length > 0) return "Course Exists";
		else {
			let newCourse = new Course({
				name: request_body.name,
				description: request_body.description,
				price: request_body.price
			})
			return newCourse.save().then((registered_course, error) => {
				if(error) return {message: error.message}
				return {status: 201, message: "Successfully registered a course!", data: registered_course};
			}).catch(error => console.log(error));
		}
	})
}

module.exports.getAllCourses = (request, response) => {
	return Course.find({}).then((result) => {
		return result;
	}).catch(error => response.send(error));
}

module.exports.getAllActiveCourses = (request, response) => {
	return Course.find({isActive: true}).then(result => {
		return result;
	}).catch(error => response.send(error));
}

module.exports.getCourse = (request, response) => {
	return Course.findById(request.params.id).then(result => {
		return result;
	}).catch(error => response.send(error));
}

module.exports.updateCourse = (request, reponse) => {
	return Course.findByIdAndUpdate(request.params.id, {
		$set: {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		}
	}, {new: true}).then(result => {
		return {message: "Course has been updated successfully", data: result};
	}).catch(error => response.send({message: error.message}));
}

module.exports.activateCourse = (request, response) => {
  return Course.findByIdAndUpdate(request.params.id, {
    $set: {
      isActive: true,
    },
  }).then((result, error) => {
    if (error) return response.send({ message: error.message });
    return response.send(true);
  });
};

module.exports.archiveCourse = (request, response) => {
  return Course.findByIdAndUpdate(request.params.id, {
    $set: {
      isActive: false,
    },
  }).then((result, error) => {
    if (error) return response.send({ message: error.message });
    return response.send(true);
  });
};

module.exports.searchCourse = (request, response) => {
	return Course.find({name: { $regex: request.body.name, $options: 'i' }}).then((result, error) => {
		if(error) return response.send({message: error.message});
		if(result.length <= 0) return response.send({message: "Search not found"});
		let selectedCourse = result.map(course => {
			return {
			Name: course.name,
			Description: course.description,
			Price: course.price,
			Active: course.isActive
		}
		})
		return response.send(selectedCourse);
	})
}

module.exports.deleteCourse = (request, response) => {
  const courseId = request.params.id;

  Course.findByIdAndDelete(courseId)
    .then((result) => {
      if (!result) {
        return response.send({ message: 'Course not found' });
      }
      return response.send({ message: 'Course deleted successfully' });
    })
    .catch((error) => response.send({ message: error.message }));
};