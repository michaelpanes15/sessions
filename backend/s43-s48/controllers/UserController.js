const User = require('../models/User.js');
const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error) return {message: error.message}
		if(result.length <= 0) return "Email does not Exist"
		else return "Email Exists"
	})
}

module.exports.registerUser = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error) return {message: error.message}
		if(result.length > 0) return "Email Exists" 
		else {
			let new_user = new User({
				firstName: request_body.firstName,
				lastName: request_body.lastName,
				email: request_body.email,
				mobileNo: request_body.mobileNo,
				password: bcrypt.hashSync(request_body.password, 10)
			})
			return new_user.save().then((registered_user, error) => {
				if(error) return {message: error.message}
				return {
					message: "Successfully registered a user!", 
					data: registered_user
				}
			}).catch(error => console.log(error));
		}
	})
}

module.exports.updateUser = (request_body) => {
	return User.findOneAndUpdate({email: request_body.email}, {
		$set: {
			firstName: request_body.firstName,
			lastName: request_body.lastName,
			email: request_body.email,
			mobileNo: request_body.mobileNo,
			password: bcrypt.hashSync(request_body.password, 10)
		}
	}, {new: true}).then((result, error) => {
		if(error) return {status: error.status, message: error.message};
		if(result != null) return {status: 201, message: result};
		else return {status: 400, message: "No Task Found"};
	})
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then((result) => {
		if(result == null) return "User is not Registered";
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
		if(isPasswordCorrect) {
			//return response.send({accessToken: auth.createAccessToken(result)})
			return {accessToken: auth.createAccessToken(result), id: result._id, meesage: true}
		}
		else return {message: "Incorrect Password"}
	}).catch(error => {message: error});
}

module.exports.getProfile = (request_body) => {
	return User.findById(request_body.id).then((result, error) => {
		if(error) return {message: error.message}
		result.password = "";
		return result;
	})
}

module.exports.enroll = async (request, response) => {
	if(request.user.isAdmin) return response.send('Action Forbidden');

	let isCourseUpdated = await Course.findById(request.body.courseId).then(course => {
		if(course == null) return {status: false, message: "Course does not exist"};

		if(!course.isActive) return {status: false, message: "Course is not Active"};
		
		let isEnrolleePresent = course.enrollees.find(enrollee => enrollee.userId === request.user.id);
		if(isEnrolleePresent !== null) return {status: false, message: "User has already enrolled to the Course"}
		
		let new_enrollee = {
			userId: request.user.id
		}
		course.enrollees.push(new_enrollee);
		return course.save().then(updated_course => {
			return {
				status: true,
				message: "Course Updated"
			}
		}).catch(error => {
			return {
				status: false, 
				message: error.message
			}
		});
	})

	if(isCourseUpdated.status !== true) return response.send(isCourseUpdated);

	let isUserUpdated = await User.findById(request.user.id).then(user => {
		let new_enrollment = {
			courseId: request.body.courseId
		}
		user.enrollments.push(new_enrollment);
		return user.save().then(updated_user => true).catch(error => error.message);
	})

	if(isUserUpdated !== true) return response.send({message: isUserUpdated});

	if(isUserUpdated && isCourseUpdated.status) return response.send({message: "Enrolled Successfully"});
}

module.exports.getEnrollments = (request, response) => {
	return User.findById(request.user.id).then((user, error) => {
		if(error) return response.send({message: error.message});
		if(user.enrollments.length == 0) return response.send({message: "User has no active Enrollments"});
		return response.send(user.enrollments);
	})
}