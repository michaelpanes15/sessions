const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/CourseController.js');
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

const {verify, verifyUser, verifyAdmin} = auth;

router.post('/addCourse', verify, verifyAdmin, (request, response) => {
	CourseController.registerCourse(request.body).then(result => {
		response.send(result);
	})
});

router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response).then(result => {
		response.send(result)
	})
});

router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response).then(result => {
		response.send(result);
	})
});

router.get('/:id', (request, response) => {
	CourseController.getCourse(request, response).then(result => {
		response.send(result);
	})
});

router.put('/:id', verify, verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response).then(result => {
		response.send(result);
	})
});

router.put("/:id/activate", verify, verifyAdmin, (request, response) => {
    CourseController.activateCourse(request, response);
  }
);

router.put("/:id/archive", verify, verifyAdmin, (request, response) => {
    CourseController.archiveCourse(request, response);
  }
);

router.post('/enroll', verify, (request, response) => {
	UserController.enroll(request, response);
});

router.post('/search', verify, (request, response) => {
	CourseController.searchCourse(request, response);
})

router.delete('/:id', verify, verifyAdmin, (request, response) => {
  CourseController.deleteCourse(request, response);
});


module.exports = router;