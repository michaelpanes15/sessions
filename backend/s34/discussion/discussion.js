db.users.insertMany([
	{
		"firstName": "Ian",
		"lastName": "Shanks",
		"age": 23,
		"contact": {
			"phone": "01978151346",
			"email": "ian.shanks@email.com"
		},
		"courses": ["CSS", "JavaScript", "Python"],
		"department": "none"
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe",
		"age": 23,
		"contact": {
			'phone': "01348794562",
			"email": "joseph.doe@gmail.com"
		},
		"courses": ["CSS", "JavaScript", "Python"],
		"department": "none"
	}
]);

db.users.insertMany({{firstName: "Ian",lastName: "Shanks",age: 23,contact: {phone: "01978151346",email: "ian.shanks@email.com"},
		'courses': ["CSS", "JavaScript", "Python"],
		"department": "none"
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe",
		"age": 23,
		"contact": {
			"phone": "01348794562",
			"email": "joseph.doe@gmail.com"
		},
		"courses": ["CSS", "JavaScript", "Python"],
		"department": "none"
	}
});

db.users.insertMany([
	{
		"firstName": "Ian",
		"lastName": "Shanks"
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe"	}
]);

db.users.findOne({"firstName":"Ian"})

db.users.updateOne(
	{
		"_id": ObjectId("64c1c0c9d4d4a1b9c56e809b")
	},
	{
		$set: {
			"lastName": "Gaza"
		}
	}
);

db.users.updateMany(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"firstName": "Mary"
		}
	}
)

db.users.deleteMany({
	"lastName":"Doe"
})

db.users.deleteOne({
	"department":"none"
})


async function findRoom(db) {
    return await (

        // Add query here
    	db.rooms.findOne({});
    );
};
