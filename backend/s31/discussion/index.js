// {
// 	"city": "Pateros",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// "cities": [
// 	{
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Batangas City",
// 		"province": "Batangas",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Star City",
// 		"province": "Pasay",
// 		"country": "Philippines"
// 		"rides": [
// 			{
// 				"name": "Star Flyer"
// 			},
// 			{
// 				"name": "Gabi ng Lagim"	
// 			}

// 		]
// 	}
// ]

let zuitt_batches = [
	{batchName: "303"},
	{batchName: "271"}
]

console.log(`Output before stringification `);
console.log(zuitt_batches);

console.log(`Output after stringification `);
console.log(JSON.stringify(zuitt_batches));

let first_name = prompt("What is your first name");
let last_name = prompt("What is your last name");

let other_data = JSON.stringify ({
	firstName: first_name,
	lastName: last_name
})

console.log(other_data);


let other_data_JSON = `[{"firstName": "Earl", "lastName": "Diaz"}]`;

let parsed_other_data = JSON.parse(other_data_JSON);

console.log(parsed_other_data);

console.log(parsed_other_data[0].firstName);