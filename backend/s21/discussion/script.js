function printName() {
	console.log("My Name is Jeff");
}

printName();

let variable_function = function() {
	console.log("Hello from function expressions!");
}

variable_function();

let global_variable = "Call me mr. worldwide";

function showNames() {
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);
}

//showname();

//console.log(showname.test);

function parentFunction() {
	let name = "Jane";

	function childFunction() {
		let nested_name = "John";

		return nested_name;
	}
	//childFunction();
	//console.log(childFunction());
	return childFunction();
}

//parentFunction();

console.log(parentFunction());


function printWelcomeMessageForUser() {
	let first_name = prompt("Enter your first name: ");
	let last_name = prompt("Enter your last name: ");

	console.log("Hello, " + first_name + " " + last_name + "!");
	console.log("Welcome sa page ko!");
}

printWelcomeMessageForUser();


