const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {
	return Task.find({}).then((result, error) => {
		if(error) return {status: error.status, message: error.message};
		return {status: 200, message: result};
	});
}

module.exports.createTask = (request_body) => {
	return Task.findOne({name: request_body.name}).then((result) => {
		if(request_body.name === "" || request_body.name === undefined || request_body.name === null) {
			return {status: 400, message: "Missing fields"};
		}
		else if(result != null && result.name == request_body.name) {
			return {status: 200, message:"Duplicate task found"};
		} 
		else {
			//Creates new instance
			let newTask = new Task({
				name: request_body.name
			});

			//Save new instance to database
			return newTask.save().then((savedTask, error) => {
				if(error) return {status: error.status, message: error.message};
				return {status: 200, message: "New Task Created"};
			})
		}}
	);
}

module.exports.deleteTask = (request_body) => {
	return Task.findOneAndDelete({name: request_body.name}).then((result, error) => {
			if(error) return {status: error.status, message: error.message};
			if(result != null) return {status: 200, message: "Task Deleted"};
			else return {status: 400, message: "No Task Found"};
		}
	);
}

//activity
module.exports.getSpecificTask = (request) => {
	return Task.findOne({_id: request.params.id}).then((result, error) => {
		if(error) return {status: error.status, message: error.message};
		if(result != null) return {status: 200, message: result};
		else return {status: 400, message: "No Task Found"};
	});
}

module.exports.updateTask = (request) => {
	return Task.findOneAndUpdate({_id: request.params.id}, {
		$set: {
			status: request.params.status
		}
	}, {new: true}).then((result, error) => {
		if(error) return {status: error.status, message: error.message};
		if(result != null) return {status: 201, message: result};
		else return {status: 400, message: "No Task Found"};
	});
}
/////