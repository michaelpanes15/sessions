let http = require("http");

// The http module has a createServer() 
// method that accepts a function as an argument 
// and allows for a creation of a server
// The arguments passed in the function are request 
// and response objects (data type) that contains 
// methods that allow us to receive requests from the 
// client and send responses back to it

http.createServer((request, response) => {
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World');
}).listen(4000);

console.log('Server is running at port 4000');