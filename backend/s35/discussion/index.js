db.users.find({
	age: {
		$gte: 82
	}
})

db.users.find({
	age: {
		$lt: 70
	}
})

db.users.find({
	firstName: {
		$regex: "s", $options: 'i'
	}
})

db.users.find({}, {
	"_id":1
})

db.employees.insertMany([
 						{
                firstName: "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "87654321",
                    email: "janedoe@gmail.com"
                },
                courses: [ "CSS", "Javascript", "Python" ],
                department: "HR"
						},
            {
                firstName: "Stephen",
                lastName: "Hawking",
                age: 76,
                contact: {
                    phone: "87654321",
                    email: "stephenhawking@gmail.com"
                },
                courses: [ "Python", "React", "PHP" ],
                department: "HR"
            },
            {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "87654321",
                    email: "neilarmstrong@gmail.com"
                },
                courses: [ "React", "Laravel", "Sass" ],
                department: "HR"
            }
]);