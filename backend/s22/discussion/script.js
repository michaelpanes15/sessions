function printName(name) {
	console.log("I'm the real " + name);
}

printName("Slim Shady");

function checkDivisibilityBy2(number) {
	let result = number % 2;
	let divisible = (number % 2 == 0) ? "it is divisible" : "it is not divisible";
	console.log("The remainer of " + number + " is " + result + " so " + divisible + " by 2");
}

checkDivisibilityBy2(prompt("Input a number"));

function createFullName(firstname, middlename, lastname) {
	console.log(firstname + " " + middlename + " " + lastname);
}

createFullName("John", "Bal", "John");

let userName = prompt("Enter your username");

function displayWelcomeMessageForUser(userName) {
	alert("Welcome back to Valorant " + userName);
}

displayWelcomeMessageForUser(userName);